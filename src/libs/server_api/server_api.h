#ifndef __CRYPTO_BANK_API_SERVER_API_H_
#define __CRYPTO_BANK_API_SERVER_API_H_

#include <string>

namespace crypto {
  namespace bank {
    namespace api {

      //account identifier
      struct account_id_t {
        unsigned char id[32];

        account_id_t(const unsigned char init_value[32]);

        bool operator < (const account_id_t & m) const {
          return memcmp(id, m.id, sizeof(id)) < 0;
        }

        static account_id_t parse(const std::string & value);
      };

      //Funds type
      typedef int64_t fund_t;

      class server_api {
      public:
        virtual void create_account(
          //the ID of the newly created account
          const account_id_t & account_id,

          //the amount of cryptocurrency
          const fund_t initialBalance) = 0;

        virtual void delete_account(
          //the ID of the deleted account
          const account_id_t & account_id) = 0;

        virtual void transfer_funds(
          //the ID of the account that money is withdrawn from
          const account_id_t & sourceAccount,

          //the ID of the account the money is transferred to
          const account_id_t & destinationAccount,

          //the amount of money transferred
          const fund_t fundsAmount) = 0;
      };
    }//namespace api
  }//namespace bank
}//namespace crypto

namespace std {

  std::string to_string(const crypto::bank::api::account_id_t & value);

}

#endif//__CRYPTO_BANK_API_SERVER_API_H_