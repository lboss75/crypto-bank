#ifndef __CRYPTO_BANK_RPC_UTILS_H_
#define __CRYPTO_BANK_RPC_UTILS_H_

#include <capnp/list.h>
#include "server_api.h"

namespace crypto {
  namespace bank {
    namespace utils {
      api::account_id_t deserialize_account_id(
        ::capnp::List<::uint8_t, ::capnp::Kind::PRIMITIVE>::Reader account_id_param);

      kj::Array<::uint8_t> seserialize_account_id(
        const api::account_id_t & value);
    }
  }
}


#endif//__CRYPTO_BANK_RPC_UTILS_H_
