#ifndef __SERVER_DATASTORAGE_BANK_MEMORY_DB_H_
#define __SERVER_DATASTORAGE_BANK_MEMORY_DB_H_

#include <map>
#include "kj/async.h"
#include "database.h"
#include "server_datastorage.h"

namespace crypto {
  namespace bank {
    namespace server {

      //Simple database in the memory
      class memory_db : public datastorage {
      public:

        kj::Promise<void> createAccount(
          const api::account_id_t & account_id,
          const api::fund_t initial_balance) override;

        kj::Promise<void> delete_account(
          const api::account_id_t & account_id) override;

        kj::Promise<void> transfer_funds(
          const api::account_id_t & sourceAccount,
          const api::account_id_t & destinationAccount,
          const api::fund_t fundsAmount) override;

      private:
        std::mutex data_mutex_;
        std::map<api::account_id_t, api::fund_t> data_;
      };
    }
  }
}


#endif//__SERVER_DATASTORAGE_BANK_DB_H_
