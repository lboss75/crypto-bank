project(crypto_bank_orm_sqlite CXX C)
cmake_minimum_required(VERSION 2.6.2)

find_package(CapnProto REQUIRED)

add_library(crypto_bank_orm_sqlite STATIC
	sqllite3/shell.c
	sqllite3/sqlite3.c
	sqllite3/sqlite3.h
	sqllite3/sqlite3ext.h
	database.cpp
	database.h
	database_orm.cpp
	database_orm.h
)

target_include_directories(crypto_bank_orm_sqlite
	PRIVATE
		${CAPNP_INCLUDE_DIRS}
		${crypto_bank_server_api_INCLUDE_DIR}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(crypto_bank_orm_sqlite
	crypto_bank_server_api
	${CMAKE_DL_LIBS}
)
