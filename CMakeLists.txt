cmake_minimum_required(VERSION 2.6.2)
project(crypto-bank)

cmake_policy(SET CMP0012 NEW)
set(CMAKE_POLICY_DEFAULT_CMP0012 NEW)

IF(MSVC)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

else()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -fprofile-arcs -ftest-coverage")

ENDIF(MSVC)

add_subdirectory(src)
add_subdirectory(tests)



