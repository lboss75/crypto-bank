#include "gtest/gtest.h"
#include "client_app.h"
#include "server_app.h"

TEST(test_rpc_core, account_id_t_parser) {
  static const unsigned char sample1[32] = {
    0xA5, 0xE6, 0x7F, 0x65, 0xA0, 0xAC, 0x48, 0xD6, 0xBC, 0x58, 0x0F, 0x87, 0x1C, 0x8B, 0x6A, 0x60,
    0xFE, 0x45, 0xBF, 0xB8, 0x72, 0x31, 0x49, 0xC4, 0x98, 0xA5, 0x15, 0x97, 0x62, 0x39, 0xCB, 0x98
  };

  static const char sample1_str[] = "A5E67F65A0AC48D6BC580F871C8B6A60FE45BFB8723149C498A515976239CB98";

  auto result1 = crypto::bank::api::account_id_t::parse(sample1_str);
  for (int i = 0; i < sizeof(sample1) / sizeof(sample1[0]); ++i) {
    GTEST_ASSERT_EQ(result1.id[i], sample1[i]);
  }

  auto result1_str = std::to_string(crypto::bank::api::account_id_t(sample1));
  GTEST_ASSERT_EQ(result1_str, sample1_str);

}