#ifndef __CRYPTO_BANK_CLIENT_APP_H__
#define __CRYPTO_BANK_CLIENT_APP_H__

#include <capnp/ez-rpc.h>
#include <kj/debug.h>

#include "bank.capnp.h"
#include "server_api.h"

namespace crypto {
  namespace bank {
    namespace client {

      class app : public api::server_api {
      public:
        app(const char * server_address);

        void create_account(
          const api::account_id_t & account_id,
          const api::fund_t initialBalance) override;

        void delete_account(
          const api::account_id_t & account_id) override;

        void transfer_funds(
          const api::account_id_t & sourceAccount,
          const api::account_id_t & destinationAccount,
          const api::fund_t fundsAmount) override;

      private:
        capnp::EzRpcClient client_;
        crypto::bank::rpc::Bank::Client api_;
        kj::WaitScope & wait_scope_;
      };
    }
  }
}


#endif//__CRYPTO_BANK_CLIENT_APP_H__