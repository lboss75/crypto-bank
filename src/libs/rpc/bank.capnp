@0xceb88505f6f51c6c;

$import "/capnp/c++.capnp".namespace("crypto::bank::rpc");

interface Bank {
	#The account identifier
	struct AccountID {
		#32-byte array
		id @0 : List(UInt8);
	}

	#create new account
	createAccount @0 (
		#the ID of the newly created account
		sourceAccount: AccountID,

		#the amount of cryptocurrency
		initialBalance : UInt64);

	#delete exists account
	deleteAccount @1 (
		#the ID of the deleting account
		sourceAccount: AccountID);

	#transfer mounts
	transferFunds @2 (
		#the ID of the account that money is withdrawn from
		sourceAccount: AccountID,
		#the ID of the account the money is transferred to
		destinationAccount: AccountID,
		#the amount of money transferred
		fundsAmount : UInt64);
}