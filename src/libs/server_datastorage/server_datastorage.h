#ifndef __SERVER_DATASTORAGE_SERVER_DATASTORAGE_H_
#define __SERVER_DATASTORAGE_SERVER_DATASTORAGE_H_

#include "server_api.h"

namespace crypto {
  namespace bank {
    namespace server {
      class datastorage {
      public:

        virtual kj::Promise<void> createAccount(
          const api::account_id_t & account_id,
          const api::fund_t initial_balance) = 0;

        virtual kj::Promise<void> delete_account(
          const api::account_id_t & account_id) = 0;

        virtual kj::Promise<void> transfer_funds(
          const api::account_id_t & sourceAccount,
          const api::account_id_t & destinationAccount,
          const api::fund_t fundsAmount) = 0;
      };
    }
  }
}


#endif//__SERVER_DATASTORAGE_SERVER_DATASTORAGE_H_