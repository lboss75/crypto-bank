#ifndef __TEST_SQL_LITE_ORM_SQL_BUILDER_TESTS_H_
#define __TEST_SQL_LITE_ORM_SQL_BUILDER_TESTS_H_

#define database mock_database
#define database_read_transaction mock_database_read_transaction
#define database_transaction mock_database_transaction
#define sql_statement mock_sql_statement

#include "database_orm.h"

class test_table1 : public crypto::bank::orm::database_table
{
public:
  test_table1()
    : database_table("test_table1"),
    column1(this, "column1"),
    column2(this, "column2")
  {
  }

  crypto::bank::orm::database_column<int> column1;
  crypto::bank::orm::database_column<std::string> column2;
};

class test_table2 : public crypto::bank::orm::database_table
{
public:
  test_table2()
    : database_table("test_table2"),
    column1(this, "column1"),
    column2(this, "column2")
  {
  }

  crypto::bank::orm::database_column<int> column1;
  crypto::bank::orm::database_column<std::string> column2;
};


#endif//__TEST_SQL_LITE_ORM_SQL_BUILDER_TESTS_H_
