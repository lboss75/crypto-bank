add_subdirectory(server_api)
add_subdirectory(rpc)
add_subdirectory(client)
add_subdirectory(server)
add_subdirectory(orm_sqlite)
add_subdirectory(server_datastorage)

