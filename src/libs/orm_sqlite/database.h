#ifndef __SQL_LITE_ORM_DATABASE_P_H_
#define __SQL_LITE_ORM_DATABASE_P_H_

#include <chrono>
#include <thread>
#include <stdexcept>
#include <string>
#include <mutex>
#include <condition_variable>
#include <queue>

#include "sqllite3/sqlite3.h"
#include <kj/async.h>
#include <kj/debug.h>
#include "server_config.h"

#ifndef _ASSERT
#define _ASSERT(x)
#endif

namespace crypto {
  namespace bank {
    namespace orm {

      class database;
      class database_transaction;

      class sql_statement
      {
      public:
        sql_statement(sqlite3 * db, const char * sql);
        sql_statement(sql_statement && original) noexcept;
        sql_statement(const sql_statement &) = delete;
        ~sql_statement();

        sql_statement & operator = (const sql_statement & ) = delete;
        sql_statement & operator = (sql_statement && original) noexcept;

        void set_parameter(int index, int value);

        void set_parameter(int index, int64_t value)
        {
          this->reset();

          sqlite3_bind_int64(this->stmt_, index, value);
        }

        void set_parameter(int index, const std::string & value);

        void set_parameter(int index, const std::chrono::system_clock::time_point & value)
        {
          this->reset();

          sqlite3_bind_int64(this->stmt_, index, std::chrono::system_clock::to_time_t(value));
        }


        bool execute();

        bool get_value(int index, int & value)
        {
          _ASSERT(read_state == this->state_);
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));

          value = sqlite3_column_int(this->stmt_, index);
          return true;
        }

        bool get_value(int index, int64_t & value)
        {
          _ASSERT(read_state == this->state_);
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));

          value = sqlite3_column_int64(this->stmt_, index);
          return true;
        }

        bool get_value(int index, std::string & value)
        {
          _ASSERT(read_state == this->state_);
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));

          auto v = (const char *)sqlite3_column_text(this->stmt_, index);
          if (nullptr == v) {
            return false;
          }

          value = v;
          return true;
        }


        bool get_value(int index, double & value)
        {
          _ASSERT(read_state == this->state_);
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));

          value = sqlite3_column_double(this->stmt_, index);
          return true;
        }

        bool get_value(int index, std::chrono::system_clock::time_point & value)
        {
          _ASSERT(read_state == this->state_);
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));

          value = std::chrono::system_clock::from_time_t(
            sqlite3_column_int64(this->stmt_, index));
          return true;
        }

        bool is_null(int index) const {
          _ASSERT(0 <= index && index < sqlite3_column_count(this->stmt_));
          return (SQLITE_NULL == sqlite3_column_type(this->stmt_, index));
        }

      private:
        //service_provider sp_;
        sqlite3 * db_;
        sqlite3_stmt * stmt_;
        //logger log_;
        //std::string query_;

        enum state_enum
        {
          bof_state,
          read_state,
          eof_state,
        };

        state_enum state_;

        void reset()
        {
          if (bof_state != this->state_) {
            sqlite3_reset(this->stmt_);
            this->state_ = bof_state;
          }
        }
      };


      class database
      {
      public:
        database();

        ~database();

        void open(const std::string& database_file)
        {
          auto error = sqlite3_open(database_file.c_str(), &this->db_);

          if (SQLITE_OK != error) {
            throw std::runtime_error(sqlite3_errmsg(this->db_));
          }

          error = sqlite3_busy_timeout(this->db_, 300000);
          if (SQLITE_OK != error) {
            throw std::runtime_error(sqlite3_errmsg(this->db_));
          }
        }

        void close()
        {
          if (nullptr != this->db_) {
            auto error = sqlite3_close(this->db_);

            if (SQLITE_OK != error) {
              auto error_msg = sqlite3_errmsg(this->db_);
              throw std::runtime_error(error_msg);
            }

            this->db_ = nullptr;
          }
        }

        void execute(const char * sql);

        sql_statement parse(const char * sql) const
        {
          return sql_statement(this->db_, sql);
        }


        /**
         * \brief This function returns the number of rows modified, inserted or deleted by
         * the most recently completed INSERT, UPDATE or DELETE statement on the database connection
         * \return Count The Number Of Rows Modified
         */
        int rows_modified() const {
          return sqlite3_changes(this->db_);
        }

        int last_insert_rowid() const {
          auto st = this->parse("select last_insert_rowid()");
          if (!st.execute()) {
            throw std::runtime_error("Invalid operation");
          }

          int result;
          if (!st.get_value(0, result)) {
            throw std::runtime_error("Invalid operation");
          }

          return result;
        }

        kj::Promise<void> begin_transaction(
          const std::function<void(database_transaction & t)> & callback);

      private:
        sqlite3 * db_;

#ifdef SQLITE_ASYNC
        struct queue_task {
          std::function<void(orm::database_transaction & t)> callback_;
          kj::PromiseFulfiller<void>& fulfiller_;


          queue_task(
            kj::PromiseFulfiller<void>& fulfiller,
            const std::function<void(orm::database_transaction & t)> & callback,
            queue_task ** selfPtr);
          
          ~queue_task();

          void done() {
            fulfiller_.fulfill();
          }

          void fail(const std::string & message) {
            fulfiller_.reject(KJ_EXCEPTION(FAILED, message));
          }
        };

        std::mutex tasks_mutex_;
        std::condition_variable tasks_cond_;
        std::queue<queue_task *> tasks_;

        std::thread task_thread_;

        void task_thread();
#endif//SQLITE_ASYNC
      };

      class database_read_transaction {
      public:
        database_read_transaction(database & db)
          : db_(db) {
        }

        sql_statement parse(const char * sql) const;

        template <typename command_type>
        sql_statement get_reader(const command_type& command) const;

      protected:
        database & db_;
      };

      class database_transaction : public database_read_transaction
      {
      public:
        database_transaction(database & db)
          : database_read_transaction(db) {
        }

        void execute(const char * sql);

        template <typename command_type>
        void execute(const command_type& command);

        int rows_modified() const;
        int last_insert_rowid() const;
      };
    }
  }
}

#endif//__VDS_DATABASE_DATABASE_P_H_
