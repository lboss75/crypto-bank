#include "database.h"

crypto::bank::orm::sql_statement::sql_statement(sqlite3 * db, const char * sql)
: db_(db), stmt_(nullptr), state_(bof_state)
{
  auto result = sqlite3_prepare_v2(db, sql, -1, &this->stmt_, nullptr);
  switch (result) {
  case SQLITE_OK:
    return;

  default:
    auto error = sqlite3_errmsg(db);
    throw std::runtime_error(error);
  }
}

crypto::bank::orm::sql_statement::sql_statement(sql_statement&& original) noexcept 
: db_(original.db_), stmt_(original.stmt_), state_(original.state_){
  original.db_ = nullptr;
  original.stmt_ = nullptr;
}

crypto::bank::orm::sql_statement& crypto::bank::orm::sql_statement::operator=(sql_statement&& original) noexcept {
  if (nullptr != this->stmt_) {
    sqlite3_finalize(this->stmt_);
  }
  db_ = original.db_;
  stmt_ = original.stmt_;
  state_ = original.state_;

  original.db_ = nullptr;
  original.stmt_ = nullptr;

  return *this;
}


crypto::bank::orm::sql_statement::~sql_statement()
{
  if (nullptr != this->stmt_) {
    sqlite3_finalize(this->stmt_);
  }
}

bool crypto::bank::orm::sql_statement::execute()
{
  auto result = sqlite3_step(this->stmt_);
  switch (result) {
  case SQLITE_ROW:
    this->state_ = read_state;
    return true;

  case SQLITE_DONE:
    this->state_ = eof_state;
    return false;

  default:
    auto error = sqlite3_errmsg(this->db_);
    throw std::runtime_error(error);
  }
}


void crypto::bank::orm::database::execute(const char * sql)
{
  char * zErrMsg = nullptr;
  auto result = sqlite3_exec(this->db_, sql, nullptr, 0, &zErrMsg);
  switch (result) {
  case SQLITE_OK:
    return;

  default:
    if (nullptr != zErrMsg) {
      std::string error_message(zErrMsg);
      sqlite3_free(zErrMsg);

      throw std::runtime_error(error_message);
    }
    else {
      throw std::runtime_error("Sqlite3 error " + std::to_string(result));
    }
  }
}

crypto::bank::orm::database::database()
: db_(nullptr) {

#ifdef SQLITE_ASYNC
  task_thread_ = std::thread([this]() {
    this->task_thread();
  });
#endif
}

crypto::bank::orm::database::~database() {
  this->close();
}


kj::Promise<void> crypto::bank::orm::database::begin_transaction(
  const std::function<void(database_transaction& t)>& callback) {
#ifdef SQLITE_ASYNC
  queue_task * task;
  auto result = kj::newAdaptedPromise<void, queue_task>(callback, &task);

  tasks_mutex_.lock();
  if(tasks_.empty()) {
    tasks_cond_.notify_one();
  }

  tasks_.push(task);
  tasks_mutex_.unlock();

  return kj::mv(result);
#else//SQLITE_ASYNC
  execute("BEGIN TRANSACTION");
  database_transaction tr(*this);
  try {
    callback(tr);
  }
  catch(...) {
    try {
      execute("ROLLBACK TRANSACTION");
    }
    catch (...) {
    }

    throw;
  }
  execute("COMMIT TRANSACTION");

  return kj::READY_NOW;
#endif//SQLITE_ASYNC
}

#ifdef SQLITE_ASYNC
crypto::bank::orm::database::queue_task::queue_task(
  kj::PromiseFulfiller<void>& fulfiller,
  const std::function<void(orm::database_transaction& t)>& callback,
  queue_task** selfPtr) 
: callback_(callback),
  fulfiller_(fulfiller){
  *selfPtr = this;
}

crypto::bank::orm::database::queue_task::~queue_task() {
}

void crypto::bank::orm::database::task_thread() {
  for(;;) {
    std::unique_lock<std::mutex> lock(tasks_mutex_);
    if(tasks_.empty()) {
      tasks_cond_.wait(lock);
      continue;
    }

    auto task = tasks_.front();
    tasks_.pop();
    lock.unlock();

    try {
      execute("BEGIN TRANSACTION");
      database_transaction tr(*this);

      task->callback_(tr);

      execute("COMMIT TRANSACTION");

      task->done();
    }
    catch (const std::exception & ex) {
      try {
        execute("ROLLBACK TRANSACTION");
      }
      catch (...) {
      }

      task->fail(ex.what());
    }
    catch (...) {

      try {
        execute("ROLLBACK TRANSACTION");
      }
      catch (...) {        
      }

      task->fail("Unexpected exception");
    }
  }
}
#endif//SQLITE_ASYNC

void crypto::bank::orm::sql_statement::set_parameter(int index, int value)
{
  this->reset();

  sqlite3_bind_int(this->stmt_, index, value);
}

void crypto::bank::orm::sql_statement::set_parameter(int index, const std::string & value)
{
  this->reset();

  sqlite3_bind_text(this->stmt_, index, value.c_str(), -1, SQLITE_TRANSIENT);
}

crypto::bank::orm::sql_statement crypto::bank::orm::database_read_transaction::parse(const char * sql) const {
  return db_.parse(sql);
}

void crypto::bank::orm::database_transaction::execute(const char* sql) {
  db_.execute(sql);
}
