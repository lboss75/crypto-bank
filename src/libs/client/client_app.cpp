#include <string>
#include <iostream>
#include <capnp/ez-rpc.h>
#include <kj/debug.h>
#include "client_app.h"
#include "utils.h"

crypto::bank::client::app::app(const char * server_address)
: client_(server_address),
  api_(client_.getMain<crypto::bank::rpc::Bank>()),
  wait_scope_(client_.getWaitScope()) {
}

void crypto::bank::client::app::create_account(const crypto::bank::api::account_id_t & account_id, crypto::bank::api::fund_t funds)
{
  auto request = api_.createAccountRequest();
  
  request.getSourceAccount().setId(utils::seserialize_account_id(account_id));
  request.setInitialBalance(funds);

  auto promise = request.send();
  promise.wait(wait_scope_);
}

void crypto::bank::client::app::delete_account(const api::account_id_t& account_id) {
  auto request = api_.deleteAccountRequest();

  request.getSourceAccount().setId(utils::seserialize_account_id(account_id));

  auto promise = request.send();
  promise.wait(wait_scope_);
}

void crypto::bank::client::app::transfer_funds(const api::account_id_t& sourceAccount,
  const api::account_id_t& destinationAccount, const api::fund_t fundsAmount) {

  auto request = api_.transferFundsRequest();

  request.getSourceAccount().setId(utils::seserialize_account_id(sourceAccount));
  request.getDestinationAccount().setId(utils::seserialize_account_id(destinationAccount));
  request.setFundsAmount(fundsAmount);

  auto promise = request.send();
  promise.wait(wait_scope_);
}
