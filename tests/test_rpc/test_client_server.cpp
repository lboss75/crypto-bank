#include "gtest/gtest.h"
#include "client_app.h"
#include "server_app.h"
#include "bank_db.h"
#include "bank_memory_db.h"
#include "server_config.h"

TEST(client_server_tests, clint_server) {
#ifdef USE_SQLITE
    crypto::bank::server::db storage;

    remove("local.db");
    storage.open("local.db");
#else
    crypto::bank::server::memory_db storage;
#endif

  crypto::bank::server::app server_app("*:8055", storage);

  crypto::bank::client::app client_app("localhost:8055");

  client_app.create_account(
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000001"),
    1000);

  client_app.create_account(
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000002"),
    1000);

  client_app.transfer_funds(
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000001"),
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000002"),
    1000);

  client_app.delete_account(
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000001"));

  client_app.delete_account(
    crypto::bank::api::account_id_t::parse(
      "0000000000000000000000000000000000000000000000000000000000000002"));
}

TEST(client_server_tests, clint_server_benchmark) {
#ifdef USE_SQLITE
    crypto::bank::server::db storage;

    remove("local.db");
    storage.open("local.db");
#else
    crypto::bank::server::memory_db storage;
#endif

    crypto::bank::server::app server_app("*:8055", storage);

  crypto::bank::client::app client_app("localhost:8055");

  static constexpr int try_count = 1000;
  const auto start1 = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < try_count; ++i) {
    char id[65];
    sprintf(id, "%064x", i);

    client_app.create_account(crypto::bank::api::account_id_t::parse(id), 1000);
  }

  const auto start2 = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < try_count; ++i) {
    char id[65];
    sprintf(id, "%064x", i);

    char dest[65];
    sprintf(dest, "%064x", i ^ 7);

    client_app.transfer_funds(
      crypto::bank::api::account_id_t::parse(id),
      crypto::bank::api::account_id_t::parse(dest),
      1000);
  }

  const auto start3 = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < try_count; ++i) {
    char id[65];
    sprintf(id, "%064x", i);

    client_app.delete_account(crypto::bank::api::account_id_t::parse(id));
  }

  const auto start4 = std::chrono::high_resolution_clock::now();

  std::cout
  << "creating: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start2 - start1).count() / try_count
  << "ns, transfering: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start3 - start2).count() / try_count
  << "ns, deleting: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start4 - start3).count() / try_count
  << "ns" << std::endl;
}