#include <string>
#include <iostream>
#include <capnp/ez-rpc.h>
#include <kj/debug.h>
#include "server.h"
#include "server_app.h"
#include "server_datastorage.h"
#include "bank_db.h"
#include "bank_memory_db.h"
#include "server_config.h"

static void show_help(const char * program) {
  std::cerr << "usage: " << program << " ADDRESS[:PORT]\n"
    "Runs the server bound to the given address/port.\n"
    "ADDRESS may be '*' to bind to all local addresses.\n"
    ":PORT may be omitted to choose a port automatically." << std::endl;
}

int main(int argc, const char* argv[]) {
  if (argc != 2) {
    show_help(argv[0]);
    return 1;
  }

  try {
#ifdef USE_SQLITE
    crypto::bank::server::db storage;
    storage.open("local.db");
#else
    crypto::bank::server::memory_db storage;
#endif
    crypto::bank::server::app app(argv[1], storage);

    auto port = app.port();
    if (port == 0) {
      // The address format "unix:/path/to/socket" opens a unix domain socket,
      // in which case the port will be zero.
      std::cout << "Listening on Unix socket..." << std::endl;
    }
    else {
      std::cout << "Listening on port " << port << "..." << std::endl;
    }

    return app.run();
  }
  catch (const std::exception & ex) {
    std::cerr << "failed: " << ex.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "Unexpected error" << std::endl;
    return 1;
  }
}

