#include <string>
#include <iostream>
#include <capnp/ez-rpc.h>
#include <kj/debug.h>
#include "bank_impl.h"
#include "utils.h"

crypto::bank::server::bank_impl::bank_impl(crypto::bank::server::datastorage & db)
  : db_(db) {
}

crypto::bank::server::bank_impl::~bank_impl() {
}


kj::Promise<void> crypto::bank::server::bank_impl::createAccount(CreateAccountContext context)
{
  const auto account_id = utils::deserialize_account_id(context.getParams().getSourceAccount().getId());

  return db_.createAccount(account_id, context.getParams().getInitialBalance());
}

kj::Promise<void> crypto::bank::server::bank_impl::deleteAccount(DeleteAccountContext context) {
  const auto account_id = utils::deserialize_account_id(context.getParams().getSourceAccount().getId());

  return db_.delete_account(account_id);
}

kj::Promise<void> crypto::bank::server::bank_impl::transferFunds(TransferFundsContext context) {
  const auto source_account_id = utils::deserialize_account_id(context.getParams().getSourceAccount().getId());
  const auto destination_account_id = utils::deserialize_account_id(context.getParams().getDestinationAccount().getId());

  return db_.transfer_funds(source_account_id, destination_account_id, context.getParams().getFundsAmount());
}
