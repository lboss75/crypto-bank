#include <string>
#include <iostream>
#include <capnp/ez-rpc.h>
#include <kj/debug.h>
#include "server_app.h"
#include "bank_impl.h"

crypto::bank::server::app::app(const char * server_address, datastorage & db)
: server_(kj::heap<bank_impl>(db), server_address),
  wait_scope_(server_.getWaitScope()) {
}
